#!/usr/bin/env bash
#
# Customized version of the original FactoryServer.sh.
# This script prevents any overwrites from the app updating (as indicated in the original script's instructions),
# and also uses appropriate variable replacements for dedicated world values from the environment of the running
# container.

set -e

SERVER_ARGS=(
	"-MultiHome=0.0.0.0"
	"-log"
	"-unattended"
)

set -x

UE_TRUE_SCRIPT_NAME=$(echo \"$0\" | xargs readlink -f)
UE_PROJECT_ROOT=$(dirname "$UE_TRUE_SCRIPT_NAME")
chmod +x "$UE_PROJECT_ROOT/Engine/Binaries/Linux/FactoryServer-Linux-Shipping"
"$UE_PROJECT_ROOT/Engine/Binaries/Linux/FactoryServer-Linux-Shipping" FactoryGame ${SERVER_ARGS[@]}
