FROM registry.gitlab.com/farvour/steam-dedicated-server-base:latest
LABEL maintainer="Thomas Farvour <tom@farvour.com>"

ARG DEBIAN_FRONTEND=noninteractive

# Perform steamcmd installations as the server user.
USER ${PROC_USER}

# This is most likely going to be the largest layer created; all the game
# files for the dedicated server. NOTE: It is a good idea to do as much as
# possible _beyond_ this point to avoid Docker having to re-create it.
RUN echo "=== Downloading and installing server with steamcmd..." \
    && ${SERVER_HOME}/Steam/steamcmd.sh \
    +force_install_dir ${SERVER_INSTALL_DIR} \
    +login anonymous \
    +app_update 1690800 validate \
    +quit

# Install custom startserver script.
COPY --chown=${PROC_USER}:${PROC_GROUP} scripts/startserver-1.sh ${SERVER_INSTALL_DIR}/

# Switch back to root user to allow entrypoint to drop privileges.
USER root

# Default game server ports.
EXPOSE 15000/tcp 15000/udp
EXPOSE 15777/tcp 15777/udp
EXPOSE 7777/tcp 7777/udp

# Install custom entrypoint script.
COPY scripts/entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/usr/bin/dumb-init", "--rewrite", "15:2", "--", "/entrypoint.sh"]
CMD ["./startserver-1.sh"]
